const express = require("express");
const router = express.Router();
const categories = require("./categoriesRoutes");
const types = require("./typesRoutes");
const products = require("./productsRoutes");
const admin = require("./adminRoutes");

router.use("/api/categories", categories);
router.use("/api/types", types);
router.use("/api/products", products);
router.use("/admin", admin);

module.exports = router;
