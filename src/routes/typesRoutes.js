const express = require("express");
const typesController = require("../controllers/typeController");

const router = express.Router();

router.get("/", typesController.retrieve);
router.get("/:id", typesController.retrieveOne);
router.post("/", typesController.create);
router.put("/:id", typesController.update);
router.delete("/:id", typesController.remove);

module.exports = router;
