const { Type } = require("../models/typeModel");
const { Product } = require("../models/productModel");

exports.retrieve = async (req, res) => {
  try {
    const type = await Type.find().sort("name");
    res.send(type);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.retrieveOne = async (req, res) => {
  try {
    const type = await Type.findById(req.params.id);

    if (!type)
      return res.status(404).send("This type with the given id doesn't exist");

    res.send(type);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.create = async (req, res) => {
  try {
    const type = new Type({
      name: req.body.name,
      categoryId: req.body.categoryId,
    });

    const result = await type.save();
    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.update = async (req, res) => {
  try {
    const type = await Type.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
        category: req.body.category,
      },
      { new: true }
    );

    if (!type)
      return res.status(404).send("This type with the given id doesn't exist");

    res.send(type);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.remove = async (req, res) => {
  try {
    const type = await Type.findByIdAndDelete(req.params.id);

    if (!type)
      return res.status(404).send("This type with the given id doesn't exist");
    const productsOfDeletedType = Product.find({ typeId: type._id });
    await Product.deleteMany({ typeId: type._id });
    res.send({
      type,
      productsOfDeletedType,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
