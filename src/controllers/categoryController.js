const { Category } = require("../models/categoryModel");
const { Type } = require("../models/typeModel");
const { Product } = require("../models/productModel");

exports.retrieve = async (req, res) => {
  try {
    const category = await Category.find().sort("name");
    res.send(category);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.retrieveOne = async (req, res) => {
  try {
    const category = await Category.findById(req.params.id);

    if (!category)
      return res
        .status(404)
        .send("This category with the given id doesn't exist");

    res.send(category);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.create = async (req, res) => {
  try {
    const category = new Category({
      name: req.body.name,
    });

    const result = await category.save();
    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.update = async (req, res) => {
  try {
    const category = await Category.findByIdAndUpdate(
      req.params.id,
      {
        name: req.body.name,
      },
      { new: true }
    );

    if (!category)
      return res
        .status(404)
        .send("This category with the given id doesn't exist");

    res.send(category);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.remove = async (req, res) => {
  try {
    const category = await Category.findByIdAndDelete(req.params.id);

    if (!category)
      return res
        .status(404)
        .send("This category with the given id doesn't exist");

    const typesOfDeletedCategory = await Type.find({
      categoryId: category._id,
    });
    await Type.deleteMany({ categoryId: category._id });

    let productsOfDeletedType = [];
    for (let i = 0; i < typesOfDeletedCategory.length; i++) {
      const type = typesOfDeletedCategory[i];
      const products = await Product.find({ typeId: type._id });
      await Product.deleteMany({ typeId: type._id });
      productsOfDeletedType.push(products);
    }
    res.send({
      category,
      typesOfDeletedCategory,
      productsOfDeletedType,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};
