const { Product } = require("../models/productModel");

exports.retrieve = async (req, res) => {
  try {
    const product = await Product.find().sort("name");
    res.send(product);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.retrieveOne = async (req, res) => {
  try {
    const product = await Product.findById(req.params.id);

    if (!product)
      return res
        .status(404)
        .send("This product with the given id doesn't exist");

    res.send(product);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.create = async (req, res) => {
  try {
    const product = new Product({
      title: req.body.title,
      detail: req.body.detail,
      typeId: req.body.typeId,
      inventory: req.body.inventory,
      rate: req.body.rate,
      price: req.body.price,
      discount: req.body.discount,
      image: req.body.image,
    });
    if (discount > price)
      res.status(404).send("Discount should be less than price.");
    const result = await product.save();
    res.send(result);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.update = async (req, res) => {
  try {
    const product = await Product.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });

    if (!product) return res.status(404).send("not found");

    res.send(user);
  } catch (error) {
    res.status(500).send(error);
  }
};

exports.remove = async (req, res) => {
  try {
    const product = await Product.findByIdAndDelete(req.params.id);

    if (!product)
      return res
        .status(404)
        .send("This product with the given id doesn't exist");

    res.send(product);
  } catch (error) {
    res.status(500).send(error);
  }
};
