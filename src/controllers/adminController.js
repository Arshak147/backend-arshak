const { Admin } = require("../models/adminModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
exports.login = async (req, res) => {
  try {
    const { username, password } = req.body;
    if (!username) res.status(400).send("Username field cannot be empty.");
    if (!password) res.status(400).send("Password field cannot be empty.");

    const admin = await Admin.findOne({ username });
    if (!admin) res.status(400).send("Invalid Username");

    const validPassword = await bcrypt.compare(password, admin.password);
    if (!validPassword) res.status(400).send("Invalid Password");
    const token = jwt.sign({ _id: admin._id }, "jwtSecondParameter", {
      expiresIn: "1h",
    });
    res.status(200).send(token);
  } catch (error) {
    res.status(500).send(error);
  }
};
exports.signUp = async (req, res) => {
  try {
    const { username, password } = req.body;
    if (!username) res.status(400).send("Enter your Username.");
    if (!password) res.status(400).send("Enter your Password");

    const salt = await bcrypt.genSalt(10);
    hashedPassword = await bcrypt.hash(password, salt);
    let newAdmin = new Admin({
      username: username,
      password: hashedPassword,
    });

    newAdmin = await newAdmin.save();
    res.status(200).send(newAdmin);
  } catch (error) {
    res.status(500).send(error);
  }
};
