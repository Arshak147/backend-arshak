const mongoose = require("mongoose");
const express = require("express");
const cors = require("cors");
const routes = require("./routes/routes");
const app = express();

mongoose
  .connect("mongodb://localhost/market")
  .then(() => console.log("Connected to MongoDB ..."))
  .catch((err) => console.log("Could not connect to MongoDB ...", err));

app.use(express.json());
app.use(cors());
app.use("/", routes);

// REGISTERING ROUTES -------------------------------

app.get("/", (req, res) => {
  res.send("Welcome");
});

// PORT SET AT 8080 ----------------------------------------

const port = process.env.PORT || 8080;

app.listen(port, () => {
  console.log("Magic happens on port " + port);
});
