const mongoose = require("mongoose");

const Type = mongoose.model(
  "Type",
  new mongoose.Schema({
    name: {
      type: String,
      required: true,
    },
    categoryId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Category",
      required: true,
    },
  })
);

exports.Type = Type;
