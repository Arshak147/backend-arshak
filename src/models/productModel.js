const mongoose = require("mongoose");

const Product = mongoose.model(
  "Product",
  mongoose.Schema({
    typeId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Type",
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    detail: {
      type: String,
      required: true,
    },
    inventory: {
      type: Number,
      required: true,
    },
    rate: {
      type: Number,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    discount: {
      type: Number,
    },
    image: {
      type: String,
      required: true,
    },
  })
);

exports.Product = Product;
